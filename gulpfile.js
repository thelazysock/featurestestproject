"use strict";

const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const watch = require('gulp-watch');
const rimraf = require('rimraf');
const browserSync = require('browser-sync');
const reload = browserSync.reload;

const path = {
  public: {
    html: 'public/',
    js: 'public/js/',
    css: 'public/css/',
    img: 'public/img/'
  },
  app: {
    html: 'app/*.html',
    js: 'app/js/**/*.js',
    scss: 'app/scss/**/*.scss',
    img: 'app/img/**/*.*'
  },
  watch: {
    html: 'app/**/*.html',
    js: 'app/js/**/*.js',
    scss: 'app/scss/**/*.scss',
    img: 'app/img/**/*.*'
  },
  clean: './public'
};

const config = {
  server: {
    baseDir: './public'
  },
  tunnel: true,
  host: 'localhost',
  port: 9000,
  logPrefix: 'test_server'
};

gulp.task('html', function() {
  gulp.src(path.app.html)
    .pipe(gulp.dest(path.public.html))
    .pipe(reload({stream: true}));
});
/*
gulp.task('js', function() {
  gulp.src(path.app.js)
    .pipe(concat('script.js'))
    .pipe(gulp.dest(path.public.js))
    .pipe(reload({stream: true}));
});
*/
gulp.task('scss', function() {
  gulp.src(path.app.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('style.css'))
    .pipe(gulp.dest(path.public.css))
    .pipe(reload({stream: true}));
});

gulp.task('img', function() {
  gulp.src(path.app.img)
    .pipe(imagemin({
      interlaced: true,
      progressive: true,
      optimizationLevel: 5,
      svgoPlugins: [{removeViewBox: true}]
    }))
    .pipe(gulp.dest(path.public.img))
    .pipe(reload({stream: true}));
});

gulp.task('build', [
    'html',
    //'js',
    'scss',
    'img'
]);

gulp.task('watch', function() {
  watch([path.watch.html], function (event, cb) {
    gulp.start('html');
  });
  /*watch([path.watch.js], function (event, cb) {
    gulp.start('js');
  });*/
  watch([path.watch.scss], function (event, cb) {
    gulp.start('scss');
  });
  watch([path.watch.img], function (event, cb) {
    gulp.start('img');
  });
});

gulp.task('server', function () {
  browserSync(config);
});

gulp.task('clean', function (cb) {
  return rimraf(path.clean, cb)
});

gulp.task('default', ['build', 'server', 'watch']);
