var reg =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ (function(module, exports) {

window.onload = function() {
  const PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let storage = {};

  let reg = new Vue ({
    el: '#reg-form' ,
    data: {
      email: '',
      name: '',
      surname: '',
      username: '',
      password: '',
      repPassword: '',
      gender: '',
      formValid: null,
    },
    created: function() {
      if (localStorage.getItem('currentUserInfo') !== 'null') {
        window.location = 'index.html';
        //console.log('regpage test passed');
      }
    },
    computed: {
      validEmail: function() {return PATTERN.test(this.email) },
      wrongName: function() { return this.name === '' },
      wrongSurname: function() { return this.surname === '' },
      wrongUsername: function() { return this.username === '' },
      wrongGender: function() { return this.gender === '' },
      wrongPassword: function() { return this.password !== this.repPassword },
      emptyPassword: function() { return this.password === ''},
      isValid: function() {
        if (!this.validEmail
          || this.wrongName
          || this.wrongSurname
          || this.wrongUsername
          || this.wrongGender
          || this.wrongPassword
          || this.emptyPassword) {
            return false;
          } else return true;
      },
    },
    methods: {
      validateForm: function(event) {
        if (this.isValid)
          {
            this.formValid = true;
            storage.email = this.email;
            storage.name = this.name;
            storage.surname = this.surname;
            storage.username = this.username;
            storage.password = this.password;
            storage.gender = this.gender;
            localStorage.setItem('currentUserInfo', JSON.stringify(storage));

            window.location = 'welcome.html';
          } else {
            event.preventDefault();
            this.formValid = false;
          }
      },
    }
  })
}


/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYjA0NDc3NjM0MjY3ZWE1MTQ0MGMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2pzL3JlZy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7OztBQzdEQTtBQUNBLHFDQUFxQyx5QkFBeUIsNkJBQTZCLElBQUksUUFBUSxJQUFJLFFBQVEsSUFBSSxRQUFRLElBQUksaUNBQWlDLEdBQUc7QUFDdks7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsOEJBQThCLGlDQUFpQztBQUMvRCw2QkFBNkIsMEJBQTBCO0FBQ3ZELGdDQUFnQyw2QkFBNkI7QUFDN0QsaUNBQWlDLDhCQUE4QjtBQUMvRCwrQkFBK0IsNEJBQTRCO0FBQzNELGlDQUFpQyw0Q0FBNEM7QUFDN0UsaUNBQWlDLDZCQUE2QjtBQUM5RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsR0FBRztBQUNIIiwiZmlsZSI6InJlZy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDEpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGIwNDQ3NzYzNDI2N2VhNTE0NDBjIiwid2luZG93Lm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG4gIGNvbnN0IFBBVFRFUk4gPSAvXigoW148PigpXFxbXFxdXFxcXC4sOzpcXHNAXCJdKyhcXC5bXjw+KClcXFtcXF1cXFxcLiw7Olxcc0BcIl0rKSopfChcIi4rXCIpKUAoKFxcW1swLTldezEsM31cXC5bMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXF0pfCgoW2EtekEtWlxcLTAtOV0rXFwuKStbYS16QS1aXXsyLH0pKSQvO1xyXG4gIGxldCBzdG9yYWdlID0ge307XHJcblxyXG4gIGxldCByZWcgPSBuZXcgVnVlICh7XHJcbiAgICBlbDogJyNyZWctZm9ybScgLFxyXG4gICAgZGF0YToge1xyXG4gICAgICBlbWFpbDogJycsXHJcbiAgICAgIG5hbWU6ICcnLFxyXG4gICAgICBzdXJuYW1lOiAnJyxcclxuICAgICAgdXNlcm5hbWU6ICcnLFxyXG4gICAgICBwYXNzd29yZDogJycsXHJcbiAgICAgIHJlcFBhc3N3b3JkOiAnJyxcclxuICAgICAgZ2VuZGVyOiAnJyxcclxuICAgICAgZm9ybVZhbGlkOiBudWxsLFxyXG4gICAgfSxcclxuICAgIGNyZWF0ZWQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICBpZiAobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRVc2VySW5mbycpICE9PSAnbnVsbCcpIHtcclxuICAgICAgICB3aW5kb3cubG9jYXRpb24gPSAnaW5kZXguaHRtbCc7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZygncmVncGFnZSB0ZXN0IHBhc3NlZCcpO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgdmFsaWRFbWFpbDogZnVuY3Rpb24oKSB7cmV0dXJuIFBBVFRFUk4udGVzdCh0aGlzLmVtYWlsKSB9LFxyXG4gICAgICB3cm9uZ05hbWU6IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpcy5uYW1lID09PSAnJyB9LFxyXG4gICAgICB3cm9uZ1N1cm5hbWU6IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpcy5zdXJuYW1lID09PSAnJyB9LFxyXG4gICAgICB3cm9uZ1VzZXJuYW1lOiBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXMudXNlcm5hbWUgPT09ICcnIH0sXHJcbiAgICAgIHdyb25nR2VuZGVyOiBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXMuZ2VuZGVyID09PSAnJyB9LFxyXG4gICAgICB3cm9uZ1Bhc3N3b3JkOiBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXMucGFzc3dvcmQgIT09IHRoaXMucmVwUGFzc3dvcmQgfSxcclxuICAgICAgZW1wdHlQYXNzd29yZDogZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzLnBhc3N3b3JkID09PSAnJ30sXHJcbiAgICAgIGlzVmFsaWQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICghdGhpcy52YWxpZEVtYWlsXHJcbiAgICAgICAgICB8fCB0aGlzLndyb25nTmFtZVxyXG4gICAgICAgICAgfHwgdGhpcy53cm9uZ1N1cm5hbWVcclxuICAgICAgICAgIHx8IHRoaXMud3JvbmdVc2VybmFtZVxyXG4gICAgICAgICAgfHwgdGhpcy53cm9uZ0dlbmRlclxyXG4gICAgICAgICAgfHwgdGhpcy53cm9uZ1Bhc3N3b3JkXHJcbiAgICAgICAgICB8fCB0aGlzLmVtcHR5UGFzc3dvcmQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgfSBlbHNlIHJldHVybiB0cnVlO1xyXG4gICAgICB9LFxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgdmFsaWRhdGVGb3JtOiBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzVmFsaWQpXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybVZhbGlkID0gdHJ1ZTtcclxuICAgICAgICAgICAgc3RvcmFnZS5lbWFpbCA9IHRoaXMuZW1haWw7XHJcbiAgICAgICAgICAgIHN0b3JhZ2UubmFtZSA9IHRoaXMubmFtZTtcclxuICAgICAgICAgICAgc3RvcmFnZS5zdXJuYW1lID0gdGhpcy5zdXJuYW1lO1xyXG4gICAgICAgICAgICBzdG9yYWdlLnVzZXJuYW1lID0gdGhpcy51c2VybmFtZTtcclxuICAgICAgICAgICAgc3RvcmFnZS5wYXNzd29yZCA9IHRoaXMucGFzc3dvcmQ7XHJcbiAgICAgICAgICAgIHN0b3JhZ2UuZ2VuZGVyID0gdGhpcy5nZW5kZXI7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdjdXJyZW50VXNlckluZm8nLCBKU09OLnN0cmluZ2lmeShzdG9yYWdlKSk7XHJcblxyXG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24gPSAnd2VsY29tZS5odG1sJztcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybVZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICB9XHJcbiAgfSlcclxufVxyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL2FwcC9qcy9yZWcuanNcbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAxIl0sInNvdXJjZVJvb3QiOiIifQ==