var todoapp =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ 3:
/***/ (function(module, exports) {

window.onload = function() {
  let todoStorage = {
    fetch: function() {
      let todos = JSON.parse(localStorage.getItem('todos') || '[]')
      todos.forEach(function(todo, index) {
        todo.id = index
      })
      todoStorage.uid = todos.length
      return todos
    },
    save: function(todos) {
      localStorage.setItem('todos', JSON.stringify(todos))
    }
  }

  // visibility filters
  let filters = {
    all: function(todos) {
      return todos
    },
    active: function(todos) {
      return todos.filter(function(todo) {
        return !todo.completed
      })
    },
    completed: function(todos) {
      return todos.filter(function(todo) {
        return todo.completed
      })
    }
  }

  // app Vue instance
  let app = new Vue({
    // app initial state
    data: {
      todos: todoStorage.fetch(),
      newTodo: '',
      editedTodo: null,
      visibility: 'all'
    },

    // watch todos change for localStorage persistence
    watch: {
      todos: {
        handler: function(todos) {
          todoStorage.save(todos)
        },
        deep: true
      }
    },

    // computed properties
    // http://vuejs.org/guide/computed.html
    computed: {
      filteredTodos: function() {
        return filters[this.visibility](this.todos)
      },
      remaining: function() {
        return filters.active(this.todos).length
      },
      allDone: {
        get: function() {
          return this.remaining === 0
        },
        set: function(value) {
          this.todos.forEach(function(todo) {
            todo.completed = value
          })
        }
      }
    },

    filters: {
      pluralize: function(n) {
        return n === 1 ? 'item' : 'items'
      }
    },

    // methods that implement data logic.
    // note there's no DOM manipulation here at all.
    methods: {
      addTodo: function() {
        let value = this.newTodo && this.newTodo.trim()
        if (!value) {
          return
        }
        this.todos.push({
          id: todoStorage.uid++,
          title: value,
          completed: false
        })
        this.newTodo = ''
      },

      removeTodo: function(todo) {
        this.todos.splice(this.todos.indexOf(todo), 1)
      },

      editTodo: function(todo) {
        this.beforeEditCache = todo.title
        this.editedTodo = todo
      },

      doneEdit: function(todo) {
        if (!this.editedTodo) {
          return
        }
        this.editedTodo = null
        todo.title = todo.title.trim()
        if (!todo.title) {
          this.removeTodo(todo)
        }
      },

      cancelEdit: function(todo) {
        this.editedTodo = null
        todo.title = this.beforeEditCache
      },

      removeCompleted: function() {
        this.todos = filters.active(this.todos)
      }
    },

    // a custom directive to wait for the DOM to be updated
    // before focusing on the input field.
    // http://vuejs.org/guide/custom-directive.html
    directives: {
      'todo-focus': function(el, binding) {
        if (binding.value) {
          el.focus()
        }
      }
    }
  })

  // handle routing
  function onHashChange() {
    let visibility = window.location.hash.replace(/#\/?/, '')
    if (filters[visibility]) {
      app.visibility = visibility
    } else {
      window.location.hash = ''
      app.visibility = 'all'
    }
  }

  window.addEventListener('hashchange', onHashChange)
  onHashChange()

  // mount
  app.$mount('#todoapp')
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgY2JlZTM5OWE1N2Q0ZDk5YTQyZjMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2pzL3RvZG9hcHAuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7QUM3REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxPQUFPOztBQUVQO0FBQ0E7QUFDQSxPQUFPOztBQUVQO0FBQ0E7QUFDQTtBQUNBLE9BQU87O0FBRVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBO0FBQ0E7QUFDQSxPQUFPOztBQUVQO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EiLCJmaWxlIjoidG9kb2FwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDMpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGNiZWUzOTlhNTdkNGQ5OWE0MmYzIiwid2luZG93Lm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG4gIGxldCB0b2RvU3RvcmFnZSA9IHtcclxuICAgIGZldGNoOiBmdW5jdGlvbigpIHtcclxuICAgICAgbGV0IHRvZG9zID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndG9kb3MnKSB8fCAnW10nKVxyXG4gICAgICB0b2Rvcy5mb3JFYWNoKGZ1bmN0aW9uKHRvZG8sIGluZGV4KSB7XHJcbiAgICAgICAgdG9kby5pZCA9IGluZGV4XHJcbiAgICAgIH0pXHJcbiAgICAgIHRvZG9TdG9yYWdlLnVpZCA9IHRvZG9zLmxlbmd0aFxyXG4gICAgICByZXR1cm4gdG9kb3NcclxuICAgIH0sXHJcbiAgICBzYXZlOiBmdW5jdGlvbih0b2Rvcykge1xyXG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgndG9kb3MnLCBKU09OLnN0cmluZ2lmeSh0b2RvcykpXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyB2aXNpYmlsaXR5IGZpbHRlcnNcclxuICBsZXQgZmlsdGVycyA9IHtcclxuICAgIGFsbDogZnVuY3Rpb24odG9kb3MpIHtcclxuICAgICAgcmV0dXJuIHRvZG9zXHJcbiAgICB9LFxyXG4gICAgYWN0aXZlOiBmdW5jdGlvbih0b2Rvcykge1xyXG4gICAgICByZXR1cm4gdG9kb3MuZmlsdGVyKGZ1bmN0aW9uKHRvZG8pIHtcclxuICAgICAgICByZXR1cm4gIXRvZG8uY29tcGxldGVkXHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG4gICAgY29tcGxldGVkOiBmdW5jdGlvbih0b2Rvcykge1xyXG4gICAgICByZXR1cm4gdG9kb3MuZmlsdGVyKGZ1bmN0aW9uKHRvZG8pIHtcclxuICAgICAgICByZXR1cm4gdG9kby5jb21wbGV0ZWRcclxuICAgICAgfSlcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIGFwcCBWdWUgaW5zdGFuY2VcclxuICBsZXQgYXBwID0gbmV3IFZ1ZSh7XHJcbiAgICAvLyBhcHAgaW5pdGlhbCBzdGF0ZVxyXG4gICAgZGF0YToge1xyXG4gICAgICB0b2RvczogdG9kb1N0b3JhZ2UuZmV0Y2goKSxcclxuICAgICAgbmV3VG9kbzogJycsXHJcbiAgICAgIGVkaXRlZFRvZG86IG51bGwsXHJcbiAgICAgIHZpc2liaWxpdHk6ICdhbGwnXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIHdhdGNoIHRvZG9zIGNoYW5nZSBmb3IgbG9jYWxTdG9yYWdlIHBlcnNpc3RlbmNlXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICB0b2Rvczoge1xyXG4gICAgICAgIGhhbmRsZXI6IGZ1bmN0aW9uKHRvZG9zKSB7XHJcbiAgICAgICAgICB0b2RvU3RvcmFnZS5zYXZlKHRvZG9zKVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVlcDogdHJ1ZVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIGNvbXB1dGVkIHByb3BlcnRpZXNcclxuICAgIC8vIGh0dHA6Ly92dWVqcy5vcmcvZ3VpZGUvY29tcHV0ZWQuaHRtbFxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgZmlsdGVyZWRUb2RvczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIGZpbHRlcnNbdGhpcy52aXNpYmlsaXR5XSh0aGlzLnRvZG9zKVxyXG4gICAgICB9LFxyXG4gICAgICByZW1haW5pbmc6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiBmaWx0ZXJzLmFjdGl2ZSh0aGlzLnRvZG9zKS5sZW5ndGhcclxuICAgICAgfSxcclxuICAgICAgYWxsRG9uZToge1xyXG4gICAgICAgIGdldDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5yZW1haW5pbmcgPT09IDBcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldDogZnVuY3Rpb24odmFsdWUpIHtcclxuICAgICAgICAgIHRoaXMudG9kb3MuZm9yRWFjaChmdW5jdGlvbih0b2RvKSB7XHJcbiAgICAgICAgICAgIHRvZG8uY29tcGxldGVkID0gdmFsdWVcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGZpbHRlcnM6IHtcclxuICAgICAgcGx1cmFsaXplOiBmdW5jdGlvbihuKSB7XHJcbiAgICAgICAgcmV0dXJuIG4gPT09IDEgPyAnaXRlbScgOiAnaXRlbXMnXHJcbiAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLy8gbWV0aG9kcyB0aGF0IGltcGxlbWVudCBkYXRhIGxvZ2ljLlxyXG4gICAgLy8gbm90ZSB0aGVyZSdzIG5vIERPTSBtYW5pcHVsYXRpb24gaGVyZSBhdCBhbGwuXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgIGFkZFRvZG86IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGxldCB2YWx1ZSA9IHRoaXMubmV3VG9kbyAmJiB0aGlzLm5ld1RvZG8udHJpbSgpXHJcbiAgICAgICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMudG9kb3MucHVzaCh7XHJcbiAgICAgICAgICBpZDogdG9kb1N0b3JhZ2UudWlkKyssXHJcbiAgICAgICAgICB0aXRsZTogdmFsdWUsXHJcbiAgICAgICAgICBjb21wbGV0ZWQ6IGZhbHNlXHJcbiAgICAgICAgfSlcclxuICAgICAgICB0aGlzLm5ld1RvZG8gPSAnJ1xyXG4gICAgICB9LFxyXG5cclxuICAgICAgcmVtb3ZlVG9kbzogZnVuY3Rpb24odG9kbykge1xyXG4gICAgICAgIHRoaXMudG9kb3Muc3BsaWNlKHRoaXMudG9kb3MuaW5kZXhPZih0b2RvKSwgMSlcclxuICAgICAgfSxcclxuXHJcbiAgICAgIGVkaXRUb2RvOiBmdW5jdGlvbih0b2RvKSB7XHJcbiAgICAgICAgdGhpcy5iZWZvcmVFZGl0Q2FjaGUgPSB0b2RvLnRpdGxlXHJcbiAgICAgICAgdGhpcy5lZGl0ZWRUb2RvID0gdG9kb1xyXG4gICAgICB9LFxyXG5cclxuICAgICAgZG9uZUVkaXQ6IGZ1bmN0aW9uKHRvZG8pIHtcclxuICAgICAgICBpZiAoIXRoaXMuZWRpdGVkVG9kbykge1xyXG4gICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZWRpdGVkVG9kbyA9IG51bGxcclxuICAgICAgICB0b2RvLnRpdGxlID0gdG9kby50aXRsZS50cmltKClcclxuICAgICAgICBpZiAoIXRvZG8udGl0bGUpIHtcclxuICAgICAgICAgIHRoaXMucmVtb3ZlVG9kbyh0b2RvKVxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuXHJcbiAgICAgIGNhbmNlbEVkaXQ6IGZ1bmN0aW9uKHRvZG8pIHtcclxuICAgICAgICB0aGlzLmVkaXRlZFRvZG8gPSBudWxsXHJcbiAgICAgICAgdG9kby50aXRsZSA9IHRoaXMuYmVmb3JlRWRpdENhY2hlXHJcbiAgICAgIH0sXHJcblxyXG4gICAgICByZW1vdmVDb21wbGV0ZWQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMudG9kb3MgPSBmaWx0ZXJzLmFjdGl2ZSh0aGlzLnRvZG9zKVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIGEgY3VzdG9tIGRpcmVjdGl2ZSB0byB3YWl0IGZvciB0aGUgRE9NIHRvIGJlIHVwZGF0ZWRcclxuICAgIC8vIGJlZm9yZSBmb2N1c2luZyBvbiB0aGUgaW5wdXQgZmllbGQuXHJcbiAgICAvLyBodHRwOi8vdnVlanMub3JnL2d1aWRlL2N1c3RvbS1kaXJlY3RpdmUuaHRtbFxyXG4gICAgZGlyZWN0aXZlczoge1xyXG4gICAgICAndG9kby1mb2N1cyc6IGZ1bmN0aW9uKGVsLCBiaW5kaW5nKSB7XHJcbiAgICAgICAgaWYgKGJpbmRpbmcudmFsdWUpIHtcclxuICAgICAgICAgIGVsLmZvY3VzKClcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9KVxyXG5cclxuICAvLyBoYW5kbGUgcm91dGluZ1xyXG4gIGZ1bmN0aW9uIG9uSGFzaENoYW5nZSgpIHtcclxuICAgIGxldCB2aXNpYmlsaXR5ID0gd2luZG93LmxvY2F0aW9uLmhhc2gucmVwbGFjZSgvI1xcLz8vLCAnJylcclxuICAgIGlmIChmaWx0ZXJzW3Zpc2liaWxpdHldKSB7XHJcbiAgICAgIGFwcC52aXNpYmlsaXR5ID0gdmlzaWJpbGl0eVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgd2luZG93LmxvY2F0aW9uLmhhc2ggPSAnJ1xyXG4gICAgICBhcHAudmlzaWJpbGl0eSA9ICdhbGwnXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignaGFzaGNoYW5nZScsIG9uSGFzaENoYW5nZSlcclxuICBvbkhhc2hDaGFuZ2UoKVxyXG5cclxuICAvLyBtb3VudFxyXG4gIGFwcC4kbW91bnQoJyN0b2RvYXBwJylcclxufVxyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL2FwcC9qcy90b2RvYXBwLmpzXG4vLyBtb2R1bGUgaWQgPSAzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCJdLCJzb3VyY2VSb290IjoiIn0=