window.onload = function() {
  let currentUserInfo = JSON.parse(localStorage.getItem('currentUserInfo'));

  let info = new Vue ({
    el: '#info',
    created: function() {
      if (localStorage.getItem('currentUserInfo') === 'null') {
        window.location = 'index.html';
        //console.log('welcome page test passed');
      }
    },
    data: {
      vueUserInfo: currentUserInfo,
    }
  })

  $('#logoff').click(function() {
    localStorage.setItem('previousUserInfo', JSON.stringify(currentUserInfo));
    localStorage.setItem('currentUserInfo', null);
    window.location = 'index.html'
  })
}
