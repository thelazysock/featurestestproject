window.onload = function() {
  let previousUserInfo = JSON.parse(localStorage.getItem('previousUserInfo'));


  let log = new Vue ({
    el: '#log-form',
    data: {
      email: '',
      password: ''
    },
    created: function() {
      if (localStorage.getItem('currentUserInfo') !== 'null') {
        window.location = 'index.html';
        //console.log('log page test passed');
      }
    },
    computed: {
      matchEmail: function() { return this.email === previousUserInfo.email },
      matchPassword: function() { return this.password === previousUserInfo.password},
      isValid: function() {
        if (this.matchEmail && this.matchPassword) {
            return true
        }
      },
    },
    methods: {
      validateForm: function(event) {
        if (this.isValid)
          {
            localStorage.setItem('currentUserInfo', JSON.stringify(previousUserInfo));
            window.location = 'welcome.html';
          } else {
            event.preventDefault();
            this.formValid = false;
          }
      },
    }
  })
}
