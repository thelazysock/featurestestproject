window.onload = function() {
  const PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let storage = {};

  let reg = new Vue ({
    el: '#reg-form' ,
    data: {
      email: '',
      name: '',
      surname: '',
      username: '',
      password: '',
      repPassword: '',
      gender: '',
      formValid: null,
    },
    created: function() {
      if (localStorage.getItem('currentUserInfo') !== 'null') {
        window.location = 'index.html';
        //console.log('regpage test passed');
      }
    },
    computed: {
      validEmail: function() {return PATTERN.test(this.email) },
      wrongName: function() { return this.name === '' },
      wrongSurname: function() { return this.surname === '' },
      wrongUsername: function() { return this.username === '' },
      wrongGender: function() { return this.gender === '' },
      wrongPassword: function() { return this.password !== this.repPassword },
      emptyPassword: function() { return this.password === ''},
      isValid: function() {
        if (!this.validEmail
          || this.wrongName
          || this.wrongSurname
          || this.wrongUsername
          || this.wrongGender
          || this.wrongPassword
          || this.emptyPassword) {
            return false;
          } else return true;
      },
    },
    methods: {
      validateForm: function(event) {
        if (this.isValid)
          {
            this.formValid = true;
            storage.email = this.email;
            storage.name = this.name;
            storage.surname = this.surname;
            storage.username = this.username;
            storage.password = this.password;
            storage.gender = this.gender;
            localStorage.setItem('currentUserInfo', JSON.stringify(storage));

            window.location = 'welcome.html';
          } else {
            event.preventDefault();
            this.formValid = false;
          }
      },
    }
  })
}
