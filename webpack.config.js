'use strict';

module.exports = {
  entry: {
      info: './app/js/info',
      reg: './app/js/reg',
      log: './app/js/log',
      todoapp: './app/js/todoapp',
      maps: './app/js/maps'
  },
  output: {
    path: __dirname + '/public/js',
    filename: '[name].js',
    library: '[name]'
  },

  watch: true,

  devtool: 'inline-source-map'

};
